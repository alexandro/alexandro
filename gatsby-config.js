// gatsby-config.js

module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: `Alejandro Quisbert Carvajal`,
    name: `Personal Blog`,
    siteUrl: `https://ale.xandro.xyz`,
    description: `Personal Blog`,

    // important to set the main text that appears in the hero
    hero: {
      heading: `Technology, music, photography and adventures`,
      maxWidth: 652,
    },
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/alexandroqc`,
      },
      {
        name: `gitlab`,
        url: `https://gitlab.com/alexandro`,
      },
      {
        name: `github`,
        url: `https://github.com/alexandroqc`,
      },
    ],
  },
  plugins: [
    {
      resolve: '@narative/gatsby-theme-novela',
      options: {
        authorsPage: true,
      },
    },
  ],
};